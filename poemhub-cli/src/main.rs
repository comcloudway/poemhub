use clap::{
    Arg,
    Command
};
use poemhub_peer::{
    User,
    Poemhub
};
use libp2p::{
    PeerId,
    Multiaddr
};
use std::str::FromStr;

fn main() {
    env_logger::init();

    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .version(env!("CARGO_PKG_VERSION"))
        .subcommand_required(true)
        .arg_required_else_help(true)
        .author(env!("CARGO_PKG_AUTHORS"))
    // register subcommand
        .subcommand(
            Command::new("register")
                .short_flag('r')
                .long_flag("register")
                .about("Create a new account returning the private key")
                .arg(
                    Arg::new("username")
                        .short('u')
                        .long("username")
                        .help("specifies the username to be used for this session")
                        .takes_value(true)
                        .required(true)
                )
        )
    // info subcommand
        .subcommand(
            Command::new("info")
                .short_flag('i')
                .long_flag("info")
                .about("Print information about your key")
                .arg(
                    Arg::new("username")
                        .short('u')
                        .long("username")
                        .help("specifies the username to be used for this session")
                        .takes_value(true)
                        .required(true)
                )
                .arg(
                    Arg::new("secret")
                        .long("secret")
                        .short('s')
                        .help("account secret")
                        .takes_value(true)
                        .required(true)
                )
                )
            // join subcommand
                .subcommand(
                    Command::new("join")
                        .short_flag('j')
                        .long_flag("join")
                        .about("Join network with precreated account")
                        .arg(
                            Arg::new("username")
                                .short('u')
                                .long("username")
                                .help("specifies the username to be used for this session")
                                .takes_value(true)
                                .required(true)
                        )
                        .arg(
                            Arg::new("secret")
                                .long("secret")
                                .short('s')
                                .help("account secret")
                                .takes_value(true)
                                .required(true)
                        )
                        .arg(
                            Arg::new("bootnodes")
                                .long("bootnodes")
                                .short('b')
                                .help("if this isn't the root instance you have to specify the ip adress of the bootnode")
                                .takes_value(true)
                                .multiple_values(true)
                        )
                        .arg(
                            Arg::new("port")
                                .long("port")
                                .short('p')
                                .help("If you are running the boot node (you normally don't) or you want a fixed port (you normally don't) you can specify a fixed port")
                                .takes_value(true)
                        )
                )
            // visit subcommand
            // used to connect to network without using an account
            // bootnodes should only be run in visit mode
                .subcommand(
                    Command::new("visit")
                        .short_flag('v')
                        .long_flag("visit")
                        .about("Used to connect to network without using an account")
                        .arg(
                            Arg::new("bootnodes")
                                .long("bootnodes")
                                .short('b')
                                .help("if this isn't the root instance you have to specify the ip adress of the bootnode")
                                .takes_value(true)
                                .multiple_values(true)
                        )
                        .arg(
                            Arg::new("port")
                                .long("port")
                                .short('p')
                                .help("If you are running the boot node (you normally don't) or you want a fixed port (you normally don't) you can specify a fixed port")
                                .takes_value(true)
                        )
                )
                .get_matches();

            match matches.subcommand() {
                Some(("register", matches)) => {
                    let username = matches.value_of("username").expect("No username provided");
                    let user = User::register(username.to_owned());
                    let key = user.export();
                    println!("
Welcome to poemhub >> {} <<\n
To be able to authenticate with poem hub, when starting the client (using join), you need to specify the following key.\n
NOTE: You should never share this key with anyone - it allows other people to post with your identity\n
Your key: {}
",
                             username,
                             key.iter().map(|n| n.to_string()).collect::<Vec<String>>().join("-"));
                }
                Some(("join", matches)) => {
                    let username = matches
                        .value_of("username")
                        .expect("No username provided")
                        .to_owned();
                    let secret = matches
                        .value_of("secret")
                        .expect("No secret key provided")
                        .split('-')
                        .map(|p| p.parse::<u8>().expect("Your secret key seems to be corrupt"))
                        .collect();

                    let user = User::login(username, secret)
                        .expect("Failed to login as the given user");

                    let mut poemhub = Poemhub::new()
                        .with_user(user);

                    if let Some(nodes) = matches.value_of("bootnodes") {
                        let nodes = nodes.split(' ')
                                         .map(|s|s.to_string())
                                         .collect::<Vec<String>>();
                        nodes
                            .iter()
                            .for_each(|node|{
                                let args: Vec<String> = node.split(';').map(|s|s.to_string()).collect();
                                let addr: Multiaddr = args.get(1).unwrap().parse().expect("Failed to parse provided bootnodes");
                                let peer_id = PeerId::from_str(args.get(0).unwrap()).unwrap();
                                poemhub = poemhub
                                    .clone()
                                    .with_bootnode((peer_id, addr));
                            });
                    }
                    if let Some(port) = matches.value_of("port") {
                        let port = port.parse::<usize>().expect("Invalid port provided");
                        poemhub = poemhub.with_port(port);
                    }

                    // connect to p2p network
                    poemhub.connect();
                    loop {
                        let stdin = std::io::stdin();
                        println!("Title:");
                        let mut title = String::new();
                        stdin.read_line(&mut title).expect("Sorry I failed to understand that");
                        println!("Content:");
                        let mut content = String::new();
                        stdin.read_line(&mut content).expect("Wow - thats above my level");
                        poemhub.post(title, content).expect("Damn that didn't work");
                    }
                },
                Some(("info", matches)) => {
                    let username = matches
                        .value_of("username")
                        .expect("No username provided")
                        .to_owned();
                    let secret = matches
                        .value_of("secret")
                        .expect("No secret key provided")
                        .split('-')
                        .map(|p| p.parse::<u8>().expect("Your secret key seems to be corrupt"))
                        .collect();

                    let user = User::login(username, secret)
                        .expect("Failed to login as the given user");

                    println!("{:?}", user.my_peer_id());
                },
                Some(("visit", matches)) => {
                    let mut poemhub = Poemhub::new();
                    if let Some(nodes) = matches.value_of("bootnodes") {
                        let nodes = nodes.split(' ')
                                         .map(|s|s.to_string())
                                         .collect::<Vec<String>>();
                        nodes
                            .iter()
                            .for_each(|node|{
                                let args: Vec<String> = node.split(';').map(|s|s.to_string()).collect();
                                let addr: Multiaddr = args.get(1).unwrap().parse().expect("Failed to parse provided bootnodes");
                                let peer_id = PeerId::from_str(args.get(0).unwrap()).unwrap();
                                poemhub = poemhub
                                    .clone()
                                    .with_bootnode((peer_id, addr));
                            });
                    }
                    if let Some(port) = matches.value_of("port") {
                        let port = port.parse::<usize>().expect("Invalid port provided");
                        poemhub = poemhub.with_port(port);
                    }

                    // connect to p2p network
                    poemhub.connect();
                    loop {
                        let mut buffer = String::new();
                        let stdin = std::io::stdin();
                        stdin.read_line(&mut buffer).expect("Well something went wrong");
                        println!("POEMS: {:?}", poemhub.get_poems());
                    }
                },
                _ => unreachable!(), // If all subcommands are defined above, anything else is unreachable
            }
        }
