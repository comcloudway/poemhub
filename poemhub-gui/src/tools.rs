use miniquad as mq;
use std::{
    sync::{
        Arc,
        Mutex
    },
    ops::Deref
};

// from https://codeberg.org/comcloudway/suitsquad/src/branch/main/src/tools.rs
// modified by comcloudway using cargo clippy

pub fn load_file(path: &str) -> Result<Vec<u8>, mq::fs::Error> {
    let cont = Arc::new(Mutex::new(None));
    let path = path.to_owned();
    {
        let cont = Arc::clone(&cont);
        mq::fs::load_file(&path, move |bytes| {
            *cont.lock().unwrap() = Some(bytes);
        });
    }
    if let Some(Ok(bytes)) = cont.lock().unwrap().deref() {
        return Ok(bytes.to_vec());
    }
    Err(mq::fs::Error::AndroidAssetLoadingError)
}
pub fn load_texture(
    ctx: &mut mq::graphics::Context,
    path: &str,
) -> Option<mq::graphics::Texture> {
    if let Ok(bytes) = load_file(path) {
        let img = image::load_from_memory(&bytes);
        if let Ok(img) = img {
            let img = img.to_rgba8();
            let width = img.width() as u16;
            let height = img.height() as u16;
            let bytes = img.into_raw();
            return Some(
                mq::Texture::from_rgba8(
                    ctx,
                    width,
                    height,
                    &bytes
                )
            );
        }
    }
    None
}
