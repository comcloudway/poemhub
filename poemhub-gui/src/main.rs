use {egui_miniquad as egui_mq, miniquad as mq};
mod tools;
use tools::load_texture;
use poemhub_peer::{
    User,
    Poemhub,
    Bootnodes
};

struct StagePopups {
    about: bool,
    welcome: bool,
    login: bool,
    register: bool,
    visit:bool
}

enum Cache {
    Login {
        stay_logged_in: bool,
        username: String,
        secret: String,
        bootnodes: Bootnodes,
        key_err: bool
    },
    Register {
        stay_logged_in: bool,
        username: String,
        bootnodes: Bootnodes,
        secret: Option<String>,
    },
    Visit {
        bootnodes: Bootnodes
    },
    Welcome,
    None,
    Poemhub {
        poem_title: String,
        poem_content: String
    }
}

struct Stage {
    /// reference to egui miniquad
    egui_mq: egui_mq::EguiMq,
    /// stores state of popups
    popups: StagePopups,
    /// base app id
    /// used for midi connection
    id: String,
    /// app name
    /// used for title bar app name
    name: String,
    /// app icon
    /// texture id
    icon: egui::TextureId,
    /// tell the update function weather it is the first loop
    first_run : bool,
    /// stores data only needed for storing data
    /// used for the current view
    /// additionally the cache is used to control the popups
    cache: Cache,
    /// access the poemhub instance
    poemhub: Option<Poemhub>
}

impl Stage {
    fn new(ctx: &mut mq::Context) -> Self {
        // setup icon
        let texture = load_texture(
            ctx,
            "icon.png",
        ).unwrap();
        let egui_tid = egui::TextureId::User(texture.gl_internal_id() as u64);
        Self {
            egui_mq: egui_mq::EguiMq::new(ctx),
            name: String::from("Poemhub"),
            id: String::from("poemhub"),
            popups: StagePopups {
                about: false,
                welcome: false,
                login: false,
                register: false,
                visit: false
            },
            icon: egui_tid,
            first_run: true,
            cache: Cache::None,
            poemhub: None
        }
    }
}

impl mq::EventHandler for Stage {
    fn update(&mut self, _ctx: &mut mq::Context) {
        if self.first_run {
            self.cache = Cache::Welcome;
            self.first_run = false;
        }

        if self.poemhub.is_some() {
            let needs_change = if let Cache::Poemhub {
                poem_title,
                poem_content
            } = &self.cache {
                poem_title.is_empty() && poem_content.is_empty()
            } else {
                true
            };

            if needs_change {
                self.cache = Cache::Poemhub {
                    poem_content: String::new(),
                    poem_title: String::new()
                };
            }
        }

        self.popups.welcome = false;
        self.popups.login = false;
        self.popups.register = false;
        self.popups.visit = false;
        match self.cache {
            Cache::Welcome => self.popups.welcome = true,
            Cache::Login {..} => self.popups.login = true,
            Cache::Register {..} => self.popups.register = true,
            Cache::Visit {..} => self.popups.visit = true,
            _ => ()
        }
    }

    fn draw(&mut self, ctx: &mut mq::Context) {
        ctx.clear(Some((1., 1., 1., 1.)), None, None);
        ctx.begin_default_pass(mq::PassAction::clear_color(0.0, 0.0, 0.0, 1.0));
        ctx.end_render_pass();

        // Run the UI code:
        self.egui_mq.run(ctx, |egui_ctx| {
            egui::TopBottomPanel::top("top_panel")
                .show(egui_ctx, |ui| {
                    egui::menu::bar(ui, |ui| {
                        ui.menu_button("File", |ui| {
                            if self.poemhub.is_some() {
                                if ui.button("Search").clicked() {

                                }
                            }

                            if ui.button("Quit").clicked() {
                                std::process::exit(0);
                            }
                        });
                        if ui.button("About").clicked() {
                            self.popups.about= !self.popups.about;
                        };
                    });
                });

            if let Some(poemhub) = &mut self.poemhub {
                if poemhub.current_user.is_some() {
                egui::SidePanel::left("post-panel")
                    .show(egui_ctx, |ui| {
                        ui.add_space(20.);
                        ui.label("I heard you wrote a poem - do you want to share it?");

                        if let Cache::Poemhub {
                            poem_title,
                            poem_content
                        } = &mut self.cache {
                            ui.label("Poem title:");
                            ui.add(egui::TextEdit::singleline(poem_title));
                            ui.label("Poem content:");
                            ui.add(egui::TextEdit::multiline(poem_content));
                            if ui.button("Post").clicked() {
                                // post poem

                                if poemhub.post(
                                    poem_title.to_string(),
                                    poem_content.to_string()
                                ).is_some() {
                                    *poem_title = String::new();
                                    *poem_content = String::new();
                                }
                            }
                            ui.label("If the poem is posted successfully, I'll clear the fields automatically - so you'll be able to post another one");
                        }
                    });
                }
            }

            egui::CentralPanel::default()
                .show(egui_ctx, |ui| {
                    if let Some(poemhub) = &self.poemhub {
                        if let Some(poems) = poemhub.get_poems() {
                            for poem in poems {
                                ui.group(|ui| {
                                    ui.heading(poem.1.title);
                                    ui.label(poem.1
                                             .content
                                             .split_at(
                                                 if poem.1.content.len() < 20 {
                                                     poem.1.content.len()
                                                 } else {
                                                     20
                                                 }).0);
                                    if ui.button("Read").clicked() {
                                        // TODO open read poem popup
                                    }
                                });
                            }
                        }
                    }
                });

            egui::Window::new("Welcome")
                .open(&mut self.popups.welcome)
                .default_width(100.0)
                .show(egui_ctx, |ui| {
                    ui.columns(2, |ui| {
                        ui[0].add(egui::Image::new(self.icon, egui::vec2(200.0,200.0)));
                        ui[0].add_space(5.0);
                        ui[0].horizontal(|ui| {
                            ui.label(format!("Welcome to {}", self.name.to_string()));
                        });
                        ui[1].vertical(|ui| {
                            ui.add_space(20.);
                            ui.label("If you are new here - why not register a new account?");
                            if ui.button("Register").clicked() {
                                self.cache = Cache::Register {
                                    stay_logged_in: false,
                                    username: String::new(),
                                    bootnodes: Vec::new(),
                                    secret: None
                                };
                            }
                            ui.add_space(10.);
                            ui.label("If you already have an account, you can use your secret to log into it:");
                            if ui.button("Login").clicked() {
                                self.cache = Cache::Login {
                                    stay_logged_in: false,
                                    username: String::new(),
                                    secret: String::new(),
                                    bootnodes: Vec::new(),
                                    key_err: false
                                };
                            }
                            ui.add_space(10.);
                            ui.label("In case you are not sure wether or not you want to create an account, to get access to the timeline");
                            if ui.button("Visit").clicked() {
                                self.cache = Cache::Visit {
                                    bootnodes: Vec::new()
                                };
                            }
                            ui.label("Just know that you need an account in order to be able to post poems");
                        });
                    });
                });

            egui::Window::new("Login")
                .open(&mut self.popups.login)
                .default_width(300.0)
                .show(egui_ctx, |ui| {
                    ui.add_space(10.);
                    if ui.button("Take me back").clicked() {
                        self.cache = Cache::Welcome;
                    }

                    ui.add_space(20.);
                    ui.label("Hello traveler,");
                    ui.label("I think I have seen you here before but I'm not sure");
                    ui.label("Either way welcome to this literary settlement");

                    if let Cache::Login {
                        stay_logged_in,
                        username,
                        secret,
                        bootnodes,
                        key_err
                    } = &mut self.cache {
                        ui.label("Could you please remind me your name again?");
                        ui.add(egui::TextEdit::singleline(username));

                        ui.add_space(10.);
                        ui.label(format!("You go by {}? Mhh yeah - I think that sounds familiar", username));

                        ui.add_space(10.);
                        ui.label("Do you, by chance, know someone from a different settlement? It would be fantastic if you could share their connection details so we could communicate with them.");
                        // TODO Bootnodes editor

                        ui.add_space(10.);
                        ui.label("Either way, before you can enter you need to show me your key:");
                        ui.add(egui::TextEdit::singleline(secret));

                        ui.add_space(10.);
                        ui.label("Mhh that key sure does look interesting");
                        if *key_err {
                            ui.label("but honestly it does not appear to be one of the keys we use around here... Do you have a different one?");
                        }
                        if ui.button("Try to login with my key").clicked() {
                            // TODO login
                            if let Ok(user) = User::login(
                                username.to_string(),
                                poemhub_peer::string_to_bytes(secret.to_string())
                            ) {

                                let mut poemhub = Poemhub::new()
                                    .with_user(user);

                                for bootnode in bootnodes {
                                    poemhub = poemhub.with_bootnode(bootnode.clone());
                                }

                                poemhub.connect();

                                self.poemhub = Some(poemhub);

                            } else {
                                *key_err = true;
                            }
                        }

                        ui.checkbox(stay_logged_in, "Allow me to keep a copy of your key, so you I won't have to check it everytime");

                    } else {
                        ui.label("Oh I'm sorry we appear to speak in different languages");
                    }

                });

            egui::Window::new("Register")
                .open(&mut self.popups.register)
                .default_width(300.)
                .default_height(100.)
                .show(egui_ctx, |ui| {
                    ui.add_space(10.);
                    if ui.button("Take me back").clicked() {
                        self.cache = Cache::Welcome;
                    }

                    ui.add_space(20.);
                    ui.label("Hello traveler,");
                    ui.label("I see you are new here");
                    ui.label("Are you perhaps trying to settle down on this literary plot of land?");

                    ui.add_space(10.);
                    if let Cache::Register {
                        username,
                        stay_logged_in,
                        bootnodes,
                        secret
                    } = &mut self.cache {
                        ui.label("How am I supposed to refer to you - dearest writer?");
                        ui.add(egui::TextEdit::singleline(username));

                        ui.add_space(10.);
                        ui.label(format!("You go by {}? Just so you know: You can come back at any point in time and change this.", username));

                        ui.add_space(10.);
                        ui.label("Do you, by chance, know someone from a different settlement? It would be fantastic if you could share their connection details so we could communicate with them.");
                        // TODO Bootnodes editor

                        ui.add_space(20.0);
                        if let Some(secret) = secret {
                            ui.label("In this box is your key. Please keep it safe from others - and make sure not to loose it, they are your only way of entry");
                            ui.collapsing(
                                "Open the box to take a look at your key",
                                |ui| {
                                    ui.add(
                                        egui::TextEdit::singleline(
                                            &mut String::from(secret.to_string())
                                        )
                                    );
                                }
                            );

                            ui.add_space(10.);
                            ui.separator();
                            ui.label("You can now access this platform. If you do not want to go through the login process all the time we can also store your key");
                            ui.checkbox(stay_logged_in, "Stay logged in");

                            if ui.button("Register").clicked() {
                                // TODO login
                                let user = User::login(
                                    username.to_string(),
                                    poemhub_peer::string_to_bytes(secret.to_string())
                                )
                                    .expect("Failed to login as the given user");

                                let mut poemhub = Poemhub::new()
                                    .with_user(user);

                                for bootnode in bootnodes {
                                    poemhub = poemhub.with_bootnode(bootnode.clone());
                                }

                                poemhub.connect();

                                self.poemhub = Some(poemhub);
                            }
                        } else {
                            ui.label("Acctually without keys we won't let you access this settlement of ours, please generate some. In case I already gave you your keys, you might want to go back and login instead.");
                            if ui.button("Hand me my keys, please").clicked() {
                                // generate keys
                                let user = User::register(username.to_owned());
                                let key = user.export();
                                *secret = Some(poemhub_peer::bytes_to_string(key));
                            }
                        }
                    } else {
                        ui.label("Oh I'm sorry we appear to speak in different languages");
                    }

                });

            egui::Window::new("Visit")
                .open(&mut self.popups.visit)
                .default_width(300.0)
                .show(egui_ctx, |ui| {
                    ui.add_space(10.);
                    if ui.button("Take me back").clicked() {
                        self.cache = Cache::Welcome;
                    }

                    ui.add_space(20.);
                    ui.label("Hello traveler,");
                    ui.label("I see you are new here");
                    ui.label("Do you want to look around before trying to settle down here?");

                    ui.add_space(10.);

                    if let Cache::Visit {
                        bootnodes,
                    } = &mut self.cache {
                        ui.add_space(10.);
                        ui.label("Do you, by chance, know someone from a different settlement? It would be fantastic if you could share their connection details so we could communicate with them.");
                        // TODO Bootnodes editor

                        if ui.button("Look around").clicked() {
                            let mut poemhub = Poemhub::new();

                            for bootnode in bootnodes {
                                poemhub = poemhub.with_bootnode(bootnode.clone());
                            }

                            poemhub.connect();

                            self.poemhub = Some(poemhub);
                        }
                    }
                    else {
                        ui.label("Oh I'm sorry we appear to speak in different languages");
                    }
                });

            egui::Window::new("About")
                .open(&mut self.popups.about)
                .default_width(100.0)
                .show(egui_ctx, |ui| {
                    ui.columns(2, |ui| {
                        ui[0].add(egui::Image::new(self.icon, egui::vec2(200.0,200.0)));
                        ui[0].add_space(5.0);
                        ui[0].horizontal(|ui| {
                            ui.label(format!("{} ({})", self.name.to_string(), self.id));
                        });
                        ui[1].vertical(|ui| {
                            ui.add_space(20.0);
                            ui.label("Authors: ".to_owned() + env!("CARGO_PKG_AUTHORS"));
                            ui.add_space(5.0);
                            ui.label("Description: ".to_owned() + env!("CARGO_PKG_DESCRIPTION"));
                            ui.add_space(5.0);
                            ui.label("Version: ".to_owned() + env!("CARGO_PKG_VERSION"));
                            ui.add_space(5.0);
                            ui.label(format!("This package is licensed under {}", env!("CARGO_PKG_LICENSE")));
                            ui.add_space(5.0);
                            ui.label("Check out the source code:");
                            ui.hyperlink(env!("CARGO_PKG_REPOSITORY"));
                        })
                    });
                });
        });

        // Draw things behind egui here

        self.egui_mq.draw(ctx);

        // Draw things in front of egui here

        ctx.commit_frame();
    }

    fn mouse_motion_event(&mut self, ctx: &mut mq::Context, x: f32, y: f32) {
        self.egui_mq.mouse_motion_event(ctx, x, y);
    }

    fn mouse_wheel_event(&mut self, ctx: &mut mq::Context, dx: f32, dy: f32) {
        self.egui_mq.mouse_wheel_event(ctx, dx, dy);
    }

    fn mouse_button_down_event(
        &mut self,
        ctx: &mut mq::Context,
        mb: mq::MouseButton,
        x: f32,
        y: f32,
    ) {
        self.egui_mq.mouse_button_down_event(ctx, mb, x, y);
    }

    fn mouse_button_up_event(
        &mut self,
        ctx: &mut mq::Context,
        mb: mq::MouseButton,
        x: f32,
        y: f32,
    ) {
        self.egui_mq.mouse_button_up_event(ctx, mb, x, y);
    }

    fn char_event(
        &mut self,
        _ctx: &mut mq::Context,
        character: char,
        _keymods: mq::KeyMods,
        _repeat: bool,
    ) {
        self.egui_mq.char_event(character);
    }

    fn key_down_event(
        &mut self,
        ctx: &mut mq::Context,
        keycode: mq::KeyCode,
        keymods: mq::KeyMods,
        _repeat: bool,
    ) {
        self.egui_mq.key_down_event(ctx, keycode, keymods);
    }

    fn key_up_event(&mut self, _ctx: &mut mq::Context, keycode: mq::KeyCode, keymods: mq::KeyMods) {
        self.egui_mq.key_up_event(keycode, keymods);
    }
}

fn main() {
    let conf = mq::conf::Conf {
        high_dpi: true,
        ..Default::default()
    };
    mq::start(conf, |mut ctx| {
        mq::UserData::owning(Stage::new(&mut ctx), ctx)
    });
}
