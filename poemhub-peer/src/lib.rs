use log::info;
use async_std::task;
use futures::{
    prelude::stream::StreamExt,
    channel:: {
        mpsc::{
            self,
            UnboundedSender,
        }
    },
    select,
};
use libp2p::{
    floodsub::{self, Floodsub, FloodsubEvent},
    identity::{
        self,
        ed25519,
        error::DecodingError
    },
    kad::{
        Kademlia,
        KademliaConfig,
        KademliaEvent,
        record::store::MemoryStore
    },
    mdns::{Mdns, MdnsConfig, MdnsEvent},
    swarm::SwarmEvent,
    Multiaddr, NetworkBehaviour, PeerId, Swarm,
    ping, Transport
};
use std::{
    collections::HashMap,
    sync::{
        Arc,
        RwLock,
    },
    thread, str::FromStr
};
use serde::{
    Deserialize,
    Serialize
};

/// a collection of a PeerId and a MultiAddress
/// used to connect to external servers
pub type Bootnode = (PeerId, Multiaddr);
/// a bootnode collection
pub type Bootnodes = Vec<Bootnode>;

/// A User
#[derive(Debug, Clone)]
pub struct User {
    /// the keypair used to recognize the given user
    /// the public key will in most cases be generated from the secret key,
    /// but both of them are kept here to save computing power
    /// the public key is used to verify the poem id and its content
    /// the private key is used to sign the poems content, the resulting has is used as the poem id
    pub keypair: ed25519::Keypair,
    /// the username is only temporary
    /// but used to post poems under this name
    /// NOTE: because it is only temporary one can change their username multiple times
    /// there can also be multiple users with the same user name
    /// the username is not bound to the keypair
    pub username: String
}
impl User {
    /// creates a new user struct
    /// used if user has not yet signed up and thus needs to create a keypair
    /// the user should either store the secret key himself
    /// or the application stores the key
    /// it is recommended to do both
    pub fn register(username: String) -> User {
        User {
            keypair: ed25519::Keypair::generate(),
            username
        }
    }
    /// creates a new user struct
    /// used if user has already created an account
    /// and can thus provide his secret
    /// a username is never permanently assigned to a private or public key
    /// it is recommended that the application stores it
    pub fn login(username: String, secret: Vec<u8>) -> Result<User, DecodingError> {
        match ed25519::SecretKey::from_bytes(secret) {
            Ok(skey) => {
                Ok(User {
                    keypair: ed25519::Keypair::from(skey),
                    username
                })
            },
            Err(e) => Err(e)
        }
    }
    /// renames the current user
    /// wont affect already posted poems
    pub fn rename(&mut self, new_username: String) {
        self.username = new_username;
    }
    /// converts the private/secret key to a Vec<u8>
    /// to be saved or displayed to the user
    pub fn export(&self) -> Vec<u8> {
        self.keypair.secret().as_ref().to_vec()
    }

    pub fn my_peer_id(&self) -> PeerId {
        PeerId::from_public_key(
            &identity::Keypair::Ed25519(self.keypair.clone())
                .public())
    }
}

/// A custom network behaviour that combines floodsub, ping and mDNS.
/// floodsub is used to broadcast messages and events
/// ping is used to keep the connection alive
/// mDNS is used for peer discovery
#[derive(NetworkBehaviour)]
#[behaviour(out_event = "PoemhubEvent")]
struct PoemhubBehaviour {
    /// for message broadcasting
    floodsub: Floodsub,
    /// for local peer discovery
    mdns: Mdns,
    /// to keep connection alive
    ping: ping::Behaviour,
    /// for bootnode based external peer discovery
    kademlia: Kademlia<MemoryStore>,

    // Struct fields which do not implement NetworkBehaviour need to be ignored
    #[behaviour(ignore)]
    #[allow(dead_code)]
    ignored_member: bool,
}
impl PoemhubBehaviour {
    pub fn new(local_peer_id: PeerId) -> Self {
        let mdns = task::block_on(Mdns::new(MdnsConfig::default())).expect("Failed to create mDNS config");
        let kademlia_config = KademliaConfig::default();

        Self {
            floodsub: Floodsub::new(local_peer_id),
            mdns,
            kademlia: Kademlia::with_config(local_peer_id, MemoryStore::new(local_peer_id), kademlia_config),
            ping: ping::Behaviour::new(ping::Config::new().with_keep_alive(true)),
            ignored_member: false,
        }
    }
}

#[derive(Debug)]
/// events that can be received by Poemhub network Behaviour
enum PoemhubEvent {
    /// events from floodsub protocol
    /// used to communicate between the peers
    Floodsub(FloodsubEvent),
    /// events from mDNS protocol
    /// used for peer discovery in local network
    Mdns(MdnsEvent),
    /// events from ping protocol
    /// used to keep connection alive
    /// no real reson to actually store them
    Ping(ping::Event),
    Kademlia(KademliaEvent),
}
impl From<MdnsEvent> for PoemhubEvent {
    fn from(v: MdnsEvent) -> Self {
        Self::Mdns(v)
    }
}
impl From<FloodsubEvent> for PoemhubEvent {
    fn from(v: FloodsubEvent) -> Self {
        Self::Floodsub(v)
    }
}
impl From<ping::Event> for PoemhubEvent {
    fn from(v: ping::Event) -> Self {
        Self::Ping(v)
    }
}
impl From<KademliaEvent> for PoemhubEvent {
    fn from(v: KademliaEvent) -> Self {
        Self::Kademlia(v)
    }
}

/// the topic using which data is broadcasted
const FLOODSUB_TOPIC: &str = "poems";

/// a poem
/// poems are identified by their hash
/// which should be used as an id
/// the has is calculated by taking the serialized Poem struct and signing it using the users secret key
/// NOTE: You have to use the USERS secret and public key NOT the local key
/// only users are allowed to post poems
/// invalid poems should not be kept
/// at the moment there is no policy that requires you to block/ban peers that repeatedly send faulty datasets
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Poem {
    /// the poems title
    pub title: String,
    /// the name of the author who wrote the poem
    pub author: String,
    /// the content of the poem
    /// usage of markdown is recommended
    pub content: String,
    /// the public key associated with the user
    /// who posted this poem
    /// the [u8;32] is created by running encode() on the public key
    public_key: [u8; 32]
}
/// represents the hash and the poem belonging to the given hash
/// the has is the poem id
/// this way one poem can not be modified or overwritten by an other user
/// as it would lead to the hash chaning
#[derive(Serialize, Deserialize)]
struct DataBundle {
    /// the serialized hashed/signed poem
    hash: String,
    poem: Poem
}
impl DataBundle {
    /// creates a new data bundle entry from the data in a hashmap
    /// can be used as a shortcut
    pub fn from_hashmap_entry(key: String, poem: Poem) -> Self {
        Self {
            hash: key,
            poem
        }
    }
}

/// data type shared between p2p thread and main thread
pub enum BridgeEvent {
    /// stops the listener
    /// effectively quitting the network
    Quit,
    /// tell the p2p thread to post a poem
    /// requires a prehashed poem
    /// and the poem itself
    Post(String, Poem),
    /// empty dummy dataset
    None
}

/// struct to manage all relevant options for connection with poemhub p2p network
#[derive(Clone)]
pub struct Poemhub {
    /// data about the user
    /// a user is only required if you want to post to poemhub
    pub current_user: Option<User>,
    /// a list of known bootnodes,
    /// should be provided by the application
    /// NOTE: you might want to fetch a bootnode list from a file server
    bootnodes: Bootnodes,
    /// the local port to run on
    /// recommended for bootnodes to be able to do port forwarding
    /// NOTE: should only be used when running as bootnode
    port: Option<usize>,
    /// the local poem database
    /// the poem id is the hash created by signing the struct
    /// using the private key assigned to the given public key
    /// this allows data sets to be validated by using the provided public key and verifying the hash(id)
    /// only valid poem datasets should be stored in this list
    poems: Arc<RwLock<HashMap<String, Poem>>>,
    bridge: Option<UnboundedSender<BridgeEvent>>
}
impl Poemhub {
    /// creates a new empty poemhub struct
    /// with peer auth data
    pub fn new() -> Self {
        Self {
            current_user: None,
            bridge: None,
            port: None,
            bootnodes: Vec::new(),
            poems: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    /// adds a specific port to the setup
    /// has to be called before connect() is being called
    pub fn with_port(mut self, port: usize) -> Self {
        self.port = Some(port);
        self
    }
    /// appends the given bootnode to the bootnodes list
    /// has to be called before connect() is being called
    pub fn with_bootnode(mut self, node: Bootnode) -> Self {
        self.bootnodes.push(node);
        self
    }
    /// sets a given user
    /// should be called before connect() is being called
    /// but idealy it also works after connect() has been called
    pub fn with_user(mut self, user: User) -> Self {
        self.current_user = Some(user);
        self
    }

    /// closes the connection to the p2p network
    /// obviously requires you to connect beforehand
    /// if closing the connection fails, an error will be thrown
    pub fn close(&mut self) -> Option<()> {
        if let Some(tx) = &mut self.bridge {
            if tx.start_send(BridgeEvent::Quit).is_ok() {
                return Some(());
            }
        }
        None
    }

    /// attempts to post a poem to the network
    /// requires you to connect to the network first
    /// NOTE: to be able to publish a poem you have to login first
    /// This function only redirects your data
    /// and thus wont throw an error as long as the data was transmitted to the second thread
    pub fn post(&mut self, title: String, content: String) -> Option<()> {
        if let Some(user) = &self.current_user {
            let author = user.username.to_owned();
            let pub_key = user.keypair.public().encode();

            let poem = Poem {
                title,
                author,
                content,
                public_key: pub_key
            };

            if let Ok(json) = serde_json::to_string(&poem) {
                let v8 = user.keypair.sign(json.as_bytes());
                let hash = bytes_to_string(v8);

                // safe poem locally
                self.poems
                    .write()
                    .expect("Failed to access local poem db")
                    .insert(hash.to_string(), poem.clone());

                let evt = BridgeEvent::Post(hash, poem);

                // send data on bridge
                if let Some(tx) = &mut self.bridge {
                    if tx.unbounded_send(evt).is_ok() {
                        return Some(());
                    }
                }
            }
        }

        None
    }

    /// returns a list of all poems in the local database
    pub fn get_poems(&self) -> Option<HashMap<String, Poem>> {
        if let Ok(db) = self.poems.read() {
            Some(db.clone())
        } else {
            None
        }
    }

    /// starts a new poemhub instance
    /// everytime you run connect a new local key will be generated
    /// NOTE: spawning multiple instances is possible - but if you do, you are not able to interact with them
    /// NOTE: Before running connect please make sure that the old instance has been stopped
    pub fn connect(&mut self) {
        let (tx, mut rx) = mpsc::unbounded::<BridgeEvent>();
        self.bridge=Some(tx);

        let bootnodes = self.bootnodes.clone();
        let port = self.port;
        let poems = self.poems.clone();
        let user = self.current_user.clone();

        thread::spawn(move ||async_std::task::block_on(async {
            let local_key = if let Some(user) = user {
                identity::Keypair::Ed25519(user.keypair)
            } else {
                identity::Keypair::generate_ed25519()
            };
            let local_peer_id = PeerId::from(local_key.public());

            info!("alive after peer key");

            // Set up an encrypted DNS-enabled TCP Transport over the Mplex and Yamux protocols
            let transport = libp2p::tcp::TcpConfig::new()
                .upgrade(libp2p::core::upgrade::Version::V1)
                .authenticate(libp2p::noise::NoiseConfig::xx(
                    libp2p::noise::Keypair::<libp2p::noise::X25519Spec>::new()
                        .into_authentic(&local_key)
                        .expect("Setting up noise encryption failed")
                ).into_authenticated())
                .multiplex(libp2p::mplex::MplexConfig::new())
                .boxed();
            info!("Alive after transport pipe");

            // Create a Floodsub topic
            let floodsub_topic = floodsub::Topic::new(FLOODSUB_TOPIC);

            // Create a Swarm to manage peers and events
            let mut swarm = {
                let mut behaviour = PoemhubBehaviour::new(local_peer_id);

                behaviour.floodsub.subscribe(floodsub_topic.clone());
                Swarm::new(transport, behaviour, local_peer_id)
            };

            // Reach out to another node if specified
            let mut i = 0;
            while let Some(to_dial) = bootnodes.get(i) {
                i+=1;
                let (peer_id, addr) = to_dial;
                    swarm
                        .behaviour_mut()
                        .kademlia
                        .add_address(
                            &peer_id,
                            addr.clone()
                        );
                    swarm.behaviour_mut()
                         .floodsub
                         .add_node_to_partial_view(peer_id.clone());
            }

            swarm
                .behaviour_mut()
                .kademlia
                .get_closest_peers(local_peer_id);
            if !bootnodes.is_empty() {
                swarm
                    .behaviour_mut()
                    .kademlia
                    .bootstrap()
                    .expect("Failed to bootstrap kadenlia");
            }

            info!("Alive after nodes");
            // Listen on all interfaces and whatever port the OS assigns
            swarm.listen_on(format!("/ip4/0.0.0.0/tcp/{}", port.unwrap_or(0)).parse().expect("Failed to parse provided port")).expect("Failed to start p2p network");

            info!("Alive after swarm start");

            loop {
                select!{
                    bridge_event = rx.select_next_some() => match bridge_event {
                        BridgeEvent::Post(hash, poem) => {

                            let dataset = DataBundle::from_hashmap_entry(hash, poem);
                            if let Ok(json) = serde_json::to_string(&dataset) {

                                swarm
                                    .behaviour_mut()
                                    .floodsub
                                    .publish(floodsub_topic.clone(), json.as_bytes())
                            } else {
                                // failed to stringify dataset
                                println!("Failed to broadcast data");
                            }
                        },
                        BridgeEvent::Quit => break,
                        BridgeEvent::None => ()
                    },

                    event = swarm.select_next_some() => match event {
                        SwarmEvent::NewListenAddr { address, .. } => {
                            println!("Listening on {:?}", address);
                        }
                        SwarmEvent::Behaviour(PoemhubEvent::Floodsub(
                            FloodsubEvent::Message(message)
                        )) => {
                            println!(
                                "Received: '{:?}' from {:?}",
                                String::from_utf8_lossy(&message.data),
                                message.source
                            );

                            // parse & validate incoming poem
                            if let Ok(json) = serde_json::from_str::<DataBundle>(&String::from_utf8_lossy(&message.data)) {
                                let DataBundle {
                                    hash,
                                    poem
                                } = json;

                                if let Ok(pub_key) = ed25519::PublicKey::decode(&poem.public_key) {
                                    if let Ok(json) = serde_json::to_string(&poem) {
                                        if pub_key.verify(json.as_bytes(), &string_to_bytes(hash.to_string())) {
                                            // poem is valid
                                            if let Ok(mut db) = poems.write() {
                                                db.insert(hash, poem);
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        SwarmEvent::Behaviour(PoemhubEvent::Floodsub(
                            FloodsubEvent::Subscribed {..}
                        )) => {
                            // loop through poem database & serialize data & broadcast
                            if let Ok(db) = poems.read() {
                                for (hash, poem) in db.iter() {
                                    println!("sharing...");
                                    if let Ok(json) = serde_json::to_string(
                                        &DataBundle::from_hashmap_entry(
                                            hash.to_owned(),
                                            poem.clone())) {
                                        swarm
                                            .behaviour_mut()
                                            .floodsub
                                            .publish(
                                                floodsub_topic.clone(),
                                                json.as_bytes())
                                    }
                                }
                            }
                        },
                        SwarmEvent::Behaviour(PoemhubEvent::Mdns(
                            MdnsEvent::Discovered(list)
                        )) => {
                            info!("found new peers");
                            for (peer, _) in list {
                                println!("Discovered {:?}", peer);
                                swarm
                                    .behaviour_mut()
                                    .floodsub
                                    .add_node_to_partial_view(peer);
                            }
                        }
                        SwarmEvent::Behaviour(PoemhubEvent::Mdns(MdnsEvent::Expired(
                            list
                        ))) => {
                            for (peer, _) in list {
                                println!("Disconnected {:?}", peer);
                                if !swarm.behaviour_mut().mdns.has_node(&peer) {
                                    swarm
                                        .behaviour_mut()
                                        .floodsub
                                        .remove_node_from_partial_view(&peer);
                                }
                            }
                        },
                        SwarmEvent::Behaviour(PoemhubEvent::Kademlia(e)) => {
                            println!("Kademlia: {:?}", e);
                        },
                        // ping events can be ignored
                        _ => {}
                    }
                }
            }

            info!("Closing thread");
        }));
    }
}

/// used to convert a hashed value back into raw binary
pub fn string_to_bytes(string: String) -> Vec<u8> {
    string
        .split('-')
        .map(|d| d.parse::<u8>().unwrap_or(0))
        .collect()
}
/// used to convert a byte array into a string
pub fn bytes_to_string(bytes: Vec<u8>) -> String {
    bytes
        .iter()
        .map(|d| d.to_string())
        .collect::<Vec<String>>()
        .join("-")
}
